import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

//Pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SecondPage } from '../pages/second/second';
import { ThirdPage } from '../pages/third/third';
import { FourthPage } from '../pages/fourth/fourth';
import { FifthPage } from '../pages/fifth/fifth';
import { SixthPage } from '../pages/sixth/sixth';

//Plugins
import { PhotoLibrary } from '@ionic-native/photo-library';
import { Keyboard } from '@ionic-native/keyboard';
import { ImagePicker } from '@ionic-native/image-picker';

//Components
import { NavigatorComponent}  from '../components/navigator/navigator'
import { ColorPicker }  from '../components/color-picker/color-picker'
import { AnimationWrapComponent }  from '../components/animation-wrap/animation-wrap'
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    NavigatorComponent,
    ColorPicker,
    AnimationWrapComponent,
    SecondPage,
    ThirdPage,
    FourthPage,
    FifthPage,
    SixthPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    LottieAnimationViewModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SecondPage,
    ThirdPage,
    FourthPage,
    FifthPage,
    SixthPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    PhotoLibrary,
    Keyboard,
    ImagePicker,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
