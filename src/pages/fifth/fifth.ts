import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SixthPage } from '../sixth/sixth';

@IonicPage()
@Component({
  selector: 'page-fifth',
  templateUrl: 'fifth.html',
})
export class FifthPage {

	pagesArray = [true, SixthPage];

	constructor(public navCtrl: NavController, public navParams: NavParams) {
	}

  	ionViewWillEnter() {
  		let el = <HTMLElement>document.getElementsByTagName("animation-wrap")[0]
	    el.style.width = window.innerWidth - 50 + "px"
	    el.style.height = window.innerWidth - 50 + "px"
  	}

}
