var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { FourthPage } from '../fourth/fourth';
import { Content } from 'ionic-angular';
//Plugins
import { Keyboard } from '@ionic-native/keyboard';
var ThirdPage = (function () {
    function ThirdPage(navCtrl, navParams, keyboard, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.keyboard = keyboard;
        this.platform = platform;
        this.pagesArray = [true, FourthPage];
    }
    ThirdPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.imageSize = window.innerWidth / 2 - 30;
        if (this.platform.is('android')) {
            this.keyboard.onKeyboardShow().subscribe(function (data) {
                var form = document.querySelector('.form');
                form.classList.add("large-padding");
                _this.content.scrollTo(0, _this.content.scrollTop + 280, 0);
            });
            this.keyboard.onKeyboardHide().subscribe(function (data) {
                var form = document.querySelector('.form');
                form.classList.remove("large-padding");
                _this.content.scrollTo(0, _this.content.scrollTop - 280, 0);
            });
        }
    };
    return ThirdPage;
}());
__decorate([
    ViewChild(Content),
    __metadata("design:type", Content)
], ThirdPage.prototype, "content", void 0);
ThirdPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-third',
        templateUrl: 'third.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, Keyboard, Platform])
], ThirdPage);
export { ThirdPage };
//# sourceMappingURL=third.js.map