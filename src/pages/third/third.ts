import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController, Content } from 'ionic-angular';

//Pages
import { FourthPage } from '../fourth/fourth';

//Plugins
import { Keyboard } from '@ionic-native/keyboard';

//Component
import { NavigatorComponent}  from '../../components/navigator/navigator'

@IonicPage()
@Component({
  	selector: 'page-third',
  	templateUrl: 'third.html',
})

export class ThirdPage {

	pagesArray = [true, FourthPage];
	navCom = new NavigatorComponent(this.navCtrl, this.view)

	@ViewChild(Content) content: Content;

  	constructor(public navCtrl: NavController, public navParams: NavParams, private keyboard: Keyboard, private platform: Platform, public view: ViewController) {
  	}

  	ionViewWillEnter() {
      let el = <HTMLElement>document.getElementsByTagName("animation-wrap")[0]
      el.style.width = (window.innerWidth/2-30) + "px"
      el.style.height = (window.innerWidth/2-30) + "px"
  		this.navCom.keyBoardFunc(this.platform, this.content, this.keyboard)
  	}

}
