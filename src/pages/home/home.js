var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SecondPage } from '../second/second';
var HomePage = (function () {
    function HomePage(navCtrl, navParams, view, imagePicker, keyboard, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.imagePicker = imagePicker;
        this.keyboard = keyboard;
        this.platform = platform;
        this.pagesArray = [false, SecondPage];
    }
    HomePage.prototype.ionViewDidLoad = function () {
        this.imageSize = window.innerWidth - 50;
    };
    HomePage.prototype.keyBoardFunctionallity = function () {
        var _this = this;
        if (this.platform.is('android')) {
            this.keyboard.onKeyboardShow().subscribe(function (data) {
                var form = document.querySelector('.form');
                form.classList.add("large-padding");
                _this.content.scrollTo(0, _this.content.scrollTop + 280, 0);
                var element = document.querySelector('navigator > div');
                element.parentElement.classList.add('hidden');
            });
            this.keyboard.onKeyboardHide().subscribe(function (data) {
                var form = document.querySelector('.form');
                form.classList.remove("large-padding");
                _this.content.scrollTo(0, _this.content.scrollTop - 280, 0);
                var element = document.querySelector('navigator > div');
                element.parentElement.classList.remove('hidden');
            });
        }
    };
    return HomePage;
}());
HomePage = __decorate([
    Component({
        selector: 'page-home',
        templateUrl: 'home.html'
    }),
    __metadata("design:paramtypes", [NavController, Object, Object, Object, Object, Object])
], HomePage);
export { HomePage };
//# sourceMappingURL=home.js.map