import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SecondPage } from '../second/second';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	pagesArray = [false, SecondPage];

	constructor(public navCtrl: NavController) {
	}

  ionViewWillEnter() {
    let el = <HTMLElement>document.getElementsByTagName("animation-wrap")[0]
    el.style.width = window.innerWidth - 50 + "px"
    el.style.height = window.innerWidth - 50 + "px"
  }

}
