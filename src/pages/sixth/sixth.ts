import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { FifthPage } from '../fifth/fifth';

@IonicPage()
@Component({
  selector: 'page-sixth',
  templateUrl: 'sixth.html',
})
export class SixthPage {

	pagesArray = [FifthPage, false];
	imageSize: number;

	  constructor(public navCtrl: NavController, public navParams: NavParams) {
	  }

    ionViewWillEnter() {
      let el = <HTMLElement>document.getElementsByTagName("animation-wrap")[0]
      el.style.width = (window.innerWidth - 50) + "px"
      el.style.height = (window.innerWidth - 50) + "px"
  	}

}
