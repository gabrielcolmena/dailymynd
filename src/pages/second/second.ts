import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform, Content } from 'ionic-angular';

//Pages
import { ThirdPage } from '../third/third';

//Plugins
import { Keyboard } from '@ionic-native/keyboard';
import { ImagePicker } from '@ionic-native/image-picker';
//Component
import { NavigatorComponent}  from '../../components/navigator/navigator'

@IonicPage()
@Component({
  	selector: 'page-second',
  	templateUrl: 'second.html',
})

export class SecondPage {

	pagesArray = [true, ThirdPage];
	selectedColor: any
  squareLogoText = "Square logo"
  longLogoText = "Long logo"
  navCom = new NavigatorComponent(this.navCtrl, this.view)

  @ViewChild(Content) content: Content;

  	constructor(public navCtrl: NavController, public navParams: NavParams, public view: ViewController, private imagePicker: ImagePicker, private keyboard: Keyboard, private platform: Platform) {
  	}

  	ionViewWillEnter() {
      this.navCom.keyBoardFunc(this.platform, this.content, this.keyboard)
      let el = <HTMLElement>document.getElementsByTagName("animation-wrap")[0]
      el.style.width = (window.innerWidth/2-30) + "px"
      el.style.height = (window.innerWidth/2-30) + "px"
  	}

  	setColor(event: any){
  		this.selectedColor = event
  	}

  	openModal(id: string){
  		let element = <HTMLElement> document.querySelector('#'+id)
  		element.classList.add('active')
  	}

  	closeModal(id: string){
  	  	let element = <HTMLElement> document.querySelector('#'+id)
  		element.classList.remove('active')

  		let input = <HTMLElement> document.querySelector("#color-input")
  		input.style.color = this.selectedColor
  	}

    openLibrary(reference: string){
      let options = {
        maximumImagesCount: 1,
        width: 500,
        height: 500,
        quality: 75
      }

      this.imagePicker.getPictures(options).then(
        file_uris => {
          console.log(file_uris)
          if (reference == 'long-logo'){ this.longLogoText = file_uris}
          else {this.squareLogoText = file_uris}
        },
        err => console.log('uh oh')
       );
    }
}
