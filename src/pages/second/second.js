var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform } from 'ionic-angular';
//Pages
import { ThirdPage } from '../third/third';
import { Content } from 'ionic-angular';
//Plugins
import { Keyboard } from '@ionic-native/keyboard';
import { ImagePicker } from '@ionic-native/image-picker';
var SecondPage = (function () {
    function SecondPage(navCtrl, navParams, view, imagePicker, keyboard, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.imagePicker = imagePicker;
        this.keyboard = keyboard;
        this.platform = platform;
        this.pagesArray = [true, ThirdPage];
        this.squareLogoText = "Square logo";
        this.longLogoText = "Long logo";
    }
    SecondPage.prototype.ionViewDidLoad = function () {
        this.imageSize = window.innerWidth / 2 - 30;
        keyBoardFunctionallity();
    };
    SecondPage.prototype.setColor = function (event) {
        this.selectedColor = event;
    };
    SecondPage.prototype.openModal = function (id) {
        var element = document.querySelector('#' + id);
        element.classList.add('active');
    };
    SecondPage.prototype.closeModal = function (id) {
        var element = document.querySelector('#' + id);
        element.classList.remove('active');
        var input = document.querySelector("#color-input");
        input.style.color = this.selectedColor;
    };
    SecondPage.prototype.openLibrary = function (reference) {
        var _this = this;
        var options = {
            maximumImagesCount: 1,
            width: 500,
            height: 500,
            quality: 75
        };
        this.imagePicker.getPictures(options).then(function (file_uris) {
            console.log(file_uris);
            if (reference == 'long-logo') {
                _this.longLogoText = file_uris;
            }
            else {
                _this.squareLogoText = file_uris;
            }
        }, function (err) { return console.log('uh oh'); });
    };
    return SecondPage;
}());
__decorate([
    ViewChild(Content),
    __metadata("design:type", Content)
], SecondPage.prototype, "content", void 0);
SecondPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-second',
        templateUrl: 'second.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, ViewController, ImagePicker, Keyboard, Platform])
], SecondPage);
export { SecondPage };
//# sourceMappingURL=second.js.map