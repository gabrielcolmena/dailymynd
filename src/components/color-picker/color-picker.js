var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Output, Input, ViewChild, EventEmitter } from '@angular/core';
var POUCH = [
    {
        START: "mousedown",
        MOVE: "mousemove",
        STOP: "mouseup"
    },
    {
        START: "touchstart",
        MOVE: "touchmove",
        STOP: "touchend"
    }
];
var ColorPicker = (function () {
    function ColorPicker() {
        this.colorChanged = new EventEmitter();
    }
    ColorPicker.prototype.ngOnInit = function () {
        if (this.hexColor) {
            this.colorFromChooser = this.hexColor;
        }
        else {
            this.colorFromChooser = "#0000FF";
        }
        this.init();
    };
    ColorPicker.prototype.init = function () {
        this.initChooser();
        this.initPalette();
    };
    ColorPicker.prototype.drawSelector = function (ctx, x, y) {
        this.drawPalette(this.colorFromChooser);
        ctx.beginPath();
        ctx.arc(x, y, 10 * this.getPixelRatio(ctx), 0, 2 * Math.PI, false);
        ctx.fillStyle = this.color;
        ctx.fill();
        ctx.lineWidth = 3;
        ctx.strokeStyle = '#FFFFFF';
        ctx.stroke();
    };
    ColorPicker.prototype.drawChooserSelector = function (ctx, x) {
        this.drawPalette(this.colorFromChooser);
        ctx.beginPath();
        ctx.arc(x, ctx.canvas.height / 2, ctx.canvas.height / 2, 0, 2 * Math.PI, false);
        ctx.fillStyle = this.colorFromChooser;
        ctx.fill();
        ctx.lineWidth = 3;
        ctx.strokeStyle = '#FFFFFF';
        ctx.stroke();
    };
    ColorPicker.prototype.initPalette = function () {
        var _this = this;
        var canvasPalette = this.palette.nativeElement;
        this.ctxPalette = canvasPalette.getContext("2d");
        var currentWidth = window.innerWidth;
        var pixelRatio = this.getPixelRatio(this.ctxPalette);
        console.log(pixelRatio);
        var width = currentWidth * 90 / 100;
        var height = width * 0.5;
        this.ctxPalette.canvas.width = width * pixelRatio;
        this.ctxPalette.canvas.height = height * pixelRatio;
        this.ctxPalette.canvas.style.width = width + "px";
        this.ctxPalette.canvas.style.height = height + "px";
        this.drawPalette(this.colorFromChooser);
        var eventChangeColor = function (event) {
            _this.updateColor(event, canvasPalette, _this.ctxPalette);
        };
        POUCH.forEach(function (pouch) {
            canvasPalette.addEventListener(pouch.START, function (event) {
                _this.drawPalette(_this.colorFromChooser);
                canvasPalette.addEventListener(pouch.MOVE, eventChangeColor);
                _this.updateColor(event, canvasPalette, _this.ctxPalette);
            });
            canvasPalette.addEventListener(pouch.STOP, function (event) {
                canvasPalette.removeEventListener(pouch.MOVE, eventChangeColor);
                _this.updateColor(event, canvasPalette, _this.ctxPalette);
                _this.drawSelector(_this.ctxPalette, _this.paletteX, _this.paletteY);
            });
        });
    };
    ColorPicker.prototype.drawPalette = function (endColor) {
        this.ctxPalette.clearRect(0, 0, this.ctxPalette.canvas.width, this.ctxPalette.canvas.height);
        var gradient = this.ctxPalette.createLinearGradient(0, 0, this.ctxPalette.canvas.width, 0);
        // Create color gradient
        gradient.addColorStop(0, "#FFFFFF");
        gradient.addColorStop(1, endColor);
        // Apply gradient to canvas
        this.ctxPalette.fillStyle = gradient;
        this.ctxPalette.fillRect(0, 0, this.ctxPalette.canvas.width, this.ctxPalette.canvas.height);
        // Create semi transparent gradient (white -> trans. -> black)
        gradient = this.ctxPalette.createLinearGradient(0, 0, 0, this.ctxPalette.canvas.height);
        gradient.addColorStop(0, "rgba(255, 255, 255, 1)");
        gradient.addColorStop(0.5, "rgba(255, 255, 255, 0)");
        gradient.addColorStop(0.5, "rgba(0,     0,   0, 0)");
        gradient.addColorStop(1, "rgba(0,     0,   0, 1)");
        // Apply gradient to canvas
        this.ctxPalette.fillStyle = gradient;
        this.ctxPalette.fillRect(0, 0, this.ctxPalette.canvas.width, this.ctxPalette.canvas.height);
    };
    ColorPicker.prototype.initChooser = function () {
        var _this = this;
        var canvasChooser = this.chooser.nativeElement;
        var ctx = canvasChooser.getContext("2d");
        var currentWidth = window.innerWidth;
        var pixelRatio = this.getPixelRatio(ctx);
        var width = currentWidth * 90 / 100;
        var height = width * 0.05;
        ctx.canvas.width = width * pixelRatio;
        ctx.canvas.height = height * pixelRatio;
        ctx.canvas.style.width = width + "px";
        ctx.canvas.style.height = height + "px";
        this.drawChooser(ctx);
        var eventChangeColorChooser = function (event) {
            _this.updateColorChooser(event, canvasChooser, ctx);
            _this.drawSelector(_this.ctxPalette, _this.ctxPalette.canvas.width, _this.ctxPalette.canvas.height / 2);
        };
        POUCH.forEach(function (pouch) {
            canvasChooser.addEventListener(pouch.START, function (event) {
                _this.drawChooser(ctx);
                canvasChooser.addEventListener(pouch.MOVE, eventChangeColorChooser);
                _this.updateColorChooser(event, canvasChooser, ctx);
                _this.drawSelector(_this.ctxPalette, _this.ctxPalette.canvas.width, _this.ctxPalette.canvas.height / 2);
            });
            canvasChooser.addEventListener(pouch.STOP, function (event) {
                canvasChooser.removeEventListener(pouch.MOVE, eventChangeColorChooser);
                _this.updateColorChooser(event, canvasChooser, ctx);
                _this.drawChooser(ctx);
                _this.drawChooserSelector(ctx, _this.chooserX);
                _this.drawSelector(_this.ctxPalette, _this.ctxPalette.canvas.width, _this.ctxPalette.canvas.height / 2);
            });
        });
    };
    ColorPicker.prototype.drawChooser = function (ctx) {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        var gradient = ctx.createLinearGradient(0, 0, ctx.canvas.width, 0);
        // Create color gradient
        gradient.addColorStop(0, "rgb(255,   0,   0)");
        gradient.addColorStop(0.15, "rgb(255,   0, 255)");
        gradient.addColorStop(0.33, "rgb(0,     0, 255)");
        gradient.addColorStop(0.49, "rgb(0,   255, 255)");
        gradient.addColorStop(0.67, "rgb(0,   255,   0)");
        gradient.addColorStop(0.84, "rgb(255, 255,   0)");
        gradient.addColorStop(1, "rgb(255,   0,   0)");
        // Apply gradient to canvas
        ctx.fillStyle = gradient;
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    };
    ColorPicker.prototype.getPixelRatio = function (ctx) {
        var dpr = window.devicePixelRatio || 1;
        var bsr = ctx.webkitBackingStorePixelRatio ||
            ctx.mozBackingStorePixelRatio ||
            ctx.msBackingStorePixelRatio ||
            ctx.oBackingStorePixelRatio ||
            ctx.backingStorePixelRatio || 1;
        return dpr / bsr;
    };
    ColorPicker.prototype.updateColorChooser = function (event, canvas, context) {
        this.color = this.colorFromChooser = this.getColor(event, canvas, context, true);
        this.colorChanged.emit(this.color);
        this.drawPalette(this.color);
    };
    ColorPicker.prototype.updateColor = function (event, canvas, context) {
        this.color = this.getColor(event, canvas, context, false);
        this.colorChanged.emit(this.color);
    };
    ColorPicker.prototype.getColor = function (event, canvas, context, fromChooser) {
        var bounding = canvas.getBoundingClientRect(), touchX = event.pageX || event.changedTouches[0].pageX || event.changedTouches[0].screenX, touchY = event.pageY || event.changedTouches[0].pageY || event.changedTouches[0].screenX;
        var x = (touchX - bounding.left) * this.getPixelRatio(context);
        var y = (touchY - bounding.top) * this.getPixelRatio(context);
        if (fromChooser) {
            this.chooserX = x;
        }
        else {
            this.paletteX = x;
            this.paletteY = y;
        }
        var imageData = context.getImageData(x, y, 1, 1);
        var red = imageData.data[0];
        var green = imageData.data[1];
        var blue = imageData.data[2];
        return "#" + this.toHex(red) + this.toHex(green) + this.toHex(blue);
    };
    ColorPicker.prototype.toHex = function (n) {
        n = parseInt(n, 10);
        if (isNaN(n))
            return "00";
        n = Math.max(0, Math.min(n, 255));
        return "0123456789ABCDEF".charAt((n - n % 16) / 16) + "0123456789ABCDEF".charAt(n % 16);
    };
    return ColorPicker;
}());
__decorate([
    Input(),
    __metadata("design:type", String)
], ColorPicker.prototype, "hexColor", void 0);
__decorate([
    Output(),
    __metadata("design:type", Object)
], ColorPicker.prototype, "colorChanged", void 0);
__decorate([
    ViewChild("palette"),
    __metadata("design:type", Object)
], ColorPicker.prototype, "palette", void 0);
__decorate([
    ViewChild("chooser"),
    __metadata("design:type", Object)
], ColorPicker.prototype, "chooser", void 0);
ColorPicker = __decorate([
    Component({
        selector: 'color-picker',
        template: " <canvas #palette style=\"background:white;\" class='center'></canvas>\n  <canvas #chooser style=\"background:white; margin-top: 20px; margin-bottom: 20px; \" class='center'></canvas>"
    })
], ColorPicker);
export { ColorPicker };
//# sourceMappingURL=color-picker.js.map