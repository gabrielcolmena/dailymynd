var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
var NavigatorComponent = (function () {
    function NavigatorComponent(navCtrl, view) {
        this.navCtrl = navCtrl;
        this.view = view;
    }
    NavigatorComponent.prototype.next = function () {
        if (this.pages[1])
            this.navCtrl.push(this.pages[1]);
        else
            console.log("unable to dismiss view");
    };
    NavigatorComponent.prototype.prev = function () {
        if (this.pages[0])
            this.view.dismiss();
        else
            console.log("unable to dismiss view");
    };
    NavigatorComponent.prototype.cancel = function () {
        this.navCtrl.popToRoot();
    };
    return NavigatorComponent;
}());
__decorate([
    Input("pages"),
    __metadata("design:type", Object)
], NavigatorComponent.prototype, "pages", void 0);
NavigatorComponent = __decorate([
    Component({
        selector: 'navigator',
        templateUrl: 'navigator.html'
    }),
    __metadata("design:paramtypes", [NavController, ViewController])
], NavigatorComponent);
export { NavigatorComponent };
//# sourceMappingURL=navigator.js.map