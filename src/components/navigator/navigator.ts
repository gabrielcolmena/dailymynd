import { Component, Input } from '@angular/core';
import { NavController, ViewController, Platform, Content } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';

@Component({
  selector: 'navigator',
  templateUrl: 'navigator.html'
})

export class NavigatorComponent {

	@Input("pages") pages;

  constructor(public navCtrl: NavController, public view: ViewController) {
  }

  next(){
  	if(this.pages[1]) this.navCtrl.push(this.pages[1])
  }

  prev(){
  	if(this.pages[0]) this.view.dismiss()
  }

  cancel(){
    this.navCtrl.popToRoot()
  }

  keyBoardFunc(platform: Platform, content: Content, keyboard: Keyboard){
        keyboard.onKeyboardShow().subscribe(data => {
          if (platform.is('android')){
            let form = <HTMLElement> document.querySelector('.form')
            form.classList.add("large-padding")
            content.scrollTo(0, content.scrollTop + 280, 500)
          }

          let nav = <HTMLElement> document.querySelector("navigator > div")
          nav.classList.add("hidden");
        })

        keyboard.onKeyboardHide().subscribe(data =>{
          if (platform.is('android')){
            let form = <HTMLElement> document.querySelector('.form')
            form.classList.remove("large-padding")
            content.scrollTo(0, content.scrollTop - 280, 500)
          }

          let nav = <HTMLElement> document.querySelector("navigator > div")
          nav.classList.remove("hidden")
        })      
  }

}
