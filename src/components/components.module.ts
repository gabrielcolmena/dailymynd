import { NgModule } from '@angular/core';
import { NavigatorComponent } from './navigator/navigator';
import { ColorPicker } from './color-picker/color-picker';

@NgModule({
	declarations: [NavigatorComponent,
    ColorPicker],
	imports: [],
	exports: [NavigatorComponent,
    ColorPicker]
})

export class ComponentsModule {}
