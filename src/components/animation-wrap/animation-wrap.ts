import {Component} from '@angular/core';

@Component({
  selector: 'animation-wrap',
  templateUrl: 'animation-wrap.html'
})

export class AnimationWrapComponent {

	animations: any;
	lottieAnimations = 
    {
      path: "assets/animation/bodymovin/data.json"
    }

	anim: any;

	handleAnimation(anim) {
		this.anim = anim;
	}

	animationOption = {
		loop: true,
		prerender: false,
		autoplay: false,
		autoloadSegments: false,
		path: this.lottieAnimations.path
	}

	animate(){
		this.anim.play()
	}

	constructor() {
		setTimeout(()=>{this.animate()},2000)
	}
}
