import { AnimationWrapComponent } from './animation-wrap';
import { NgModule } from '@angular/core';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
    declarations: [
        AnimationWrapComponent,
    ],
    imports: [
        LottieAnimationViewModule.forRoot()
    ],
    exports: [
        AnimationWrapComponent
    ]
})

export class HomePageModule { };